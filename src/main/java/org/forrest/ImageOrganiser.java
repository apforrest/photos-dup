package org.forrest;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.ExifSubIFDDirectory;
import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.control.Either;
import io.vavr.control.Try;
import org.apache.commons.cli.*;
import org.apache.log4j.Logger;
import org.forrest.Reports.*;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.*;

public final class ImageOrganiser {

    private static final Logger logger = Logger.getLogger(ImageOrganiser.class);

    private static final double ACCEPTABLE_DIFF = 3.5d;

    private static final DateTimeFormatter formatter = new DateTimeFormatterBuilder()
            .appendLiteral("img")
            .appendPattern("yyyyMMddHHmmss")
            .toFormatter();
    private static final ReportVisitor reportVisitor = new LogReportVisitor();

    public static void main(String... args) throws IOException, ParseException {
        CommandLine line = parseOptions(args);
        List<Path> source = Arrays.stream(line.getOptionValues('s'))
                .map(Paths::get)
                .peek(file -> {
                    if (Files.notExists(file)) {
                        throw new IllegalArgumentException("Source path does not exist: " + file);
                    }
                })
                .collect(toList());

        new ImageOrganiser().start(source);
    }

    private static CommandLine parseOptions(String... args) throws ParseException {
        Options options = new Options();
        options.addOption(Option.builder("s")
                .longOpt("source")
                .type(String.class)
                .required()
                .argName("path")
                .hasArgs()
                .valueSeparator(' ')
                .desc("Source location for photos to restructure")
                .build());

        try {
            CommandLineParser parser = new DefaultParser();
            return parser.parse(options, args);
        } catch (ParseException pE) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("Image Organiser", options);
            throw pE;
        }
    }

    private void start(List<Path> sources) {
        List<Path> files = getFilesWithin(sources);

        if (logger.isDebugEnabled()) {
            logger.debug("Found " + files.size() + " file(s) in total");
        }

        Tuple2<List<Tuple2<LocalDateTime, Path>>, List<ReportEntry>> parsedImagesWithReport = files.stream()
                .parallel()
                .map(this::extractCreationDate)
                .map(either -> either.fold(left -> Tuple.of(singletonList(left), Collections.<ReportEntry>emptyList()),
                        right -> Tuple.of(Collections.<Tuple2<LocalDateTime, Path>>emptyList(), singletonList(right))))
                .reduce((t1, t2) -> Tuple.of(merge(t1._1, t2._1), merge(t1._2, t2._2)))
                .orElseThrow(() -> new IllegalArgumentException("Unable to process images"));

        Map<LocalDateTime, List<Path>> imagesIndexedByCreationDate = parsedImagesWithReport._1.stream()
                .parallel()
                .collect(groupingByConcurrent(Tuple2::_1, mapping(Tuple2::_2, toList())));

        Stream<ReportEntry> reports = imagesIndexedByCreationDate.entrySet().stream()
                .parallel()
                .filter(entry -> entry.getValue().size() > 1)
                .map(entry -> identifyDuplicates(entry.getValue()))
                .flatMap(Collection::stream);

        Map<Class<? extends ReportEntry>, List<ReportEntry>> groupedReports =
                Stream.concat(parsedImagesWithReport._2.stream(), reports)
                        .parallel()
                        .collect(groupingByConcurrent(ReportEntry::getType, mapping(Function.identity(), toList())));

        logReports(groupedReports);
    }

    private List<Path> getFilesWithin(List<Path> sources) {
        return sources.stream()
                .parallel()
                .map(source ->
                        Try.withResources(() -> Files.walk(source))
                                .of(files -> files.filter(Files::isRegularFile).collect(toList()))
                                .onFailure(e -> logger.error("Failed to read files from " + source, e))
                                .peek(files -> logger.debug("Read " + files.size() + " file(s) from " + source))
                                .getOrElse(Collections::emptyList))
                .flatMap(Collection::stream)
                .collect(toList());
    }

    private Either<Tuple2<LocalDateTime, Path>, ReportEntry> extractCreationDate(Path file) {
        logger.debug("Attempting to extract creation date from exif data in " + file);
        try (InputStream stream = Files.newInputStream(file)) {
            Metadata metadata = ImageMetadataReader.readMetadata(stream);

            return Optional.ofNullable(metadata.getFirstDirectoryOfType(ExifSubIFDDirectory.class))
                    .map(ExifSubIFDDirectory::getDateOriginal)
                    .map(creationDate -> creationDate.toInstant().atZone(ZoneId.systemDefault()))
                    .map(ZonedDateTime::toLocalDateTime)
                    .map(creationDateTime -> Either
                            .<Tuple2<LocalDateTime, Path>, ReportEntry>left(Tuple.of(creationDateTime, file)))
                    .orElseGet(() -> Either.right(Reports.error("Unable to extract creation date from " + file)));

        } catch (IOException | ImageProcessingException e) {
            return Either.right(Reports.error("Unable to process file " + file, e));
        }
    }

    private List<ReportEntry> identifyDuplicates(List<Path> images) {
        return IntStream.range(0, images.size())
                .mapToObj(index -> Tuple.of(index, images.get(index)))
                .map(image -> images.subList(image._1 + 1, images.size()).stream()
                        .map(other -> compareImages(image._2, other))
                        .filter(Optional::isPresent)
                        .map(Optional::get)
                        .collect(Collectors.toList()))
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    private Optional<ReportEntry> compareImages(Path image, Path other) {
        return getDiff(image, other).fold(diff -> (diff < ACCEPTABLE_DIFF)
                        ? Optional.of(Reports.duplicate(image, other, diff))
                        : Optional.empty(),
                Optional::of);
    }

    private Either<Double, ReportEntry> getDiff(Path image, Path other) {
        try {
            return Either.left(ImgDiffPercent.diff(image.toFile(), other.toFile()));
        } catch (IOException e) {
            return Either.right(Reports.error("Failed to calculate diff", e));
        }
    }

    private void logReports(Map<Class<? extends ReportEntry>, List<ReportEntry>> groupedReports) {
        if (logger.isDebugEnabled()) {
            logReportGroup(groupedReports.get(SameImageReportEntry.class),
                    count -> logger.debug("Images already organised (" + count + "):"));
        }

        logReportGroup(groupedReports.get(DuplicateImageReportEntry.class),
                count -> logger.warn("Duplicate images (" + count + "):"));
        logReportGroup(groupedReports.get(ErrorReportEntry.class),
                count -> logger.error("Errors (" + count + "):"));
    }

    private void logReportGroup(List<ReportEntry> reportGroup, Consumer<Integer> reportCount) {
        Optional.ofNullable(reportGroup)
                .ifPresent(reports -> {
                    reportCount.accept(reports.size());
                    reports.forEach(r -> r.accept(reportVisitor));
                });
    }

    private <T> List<T> merge(List<T> list1, List<T> list2) {
        return Stream.concat(list1.stream(), list2.stream())
                .collect(Collectors.toList());
    }

    private static final class LogReportVisitor implements ReportVisitor {

        @Override
        public void visit(SameImageReportEntry entry) {
            logger.debug("Already organised: " + entry.image);
        }

        @Override
        public void visit(DuplicateImageReportEntry entry) {
            logger.warn("Duplicate: " + entry.image + ", existing: "
                    + entry.existingImage + " (diff: " + entry.diff + "%)");
        }

        @Override
        public void visit(ErrorReportEntry entry) {
            logger.error("Error: " + entry.errorMessage, entry.exception);
        }

    }

}
