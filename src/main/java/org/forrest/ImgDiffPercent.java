package org.forrest;

import static java.lang.Math.abs;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

final class ImgDiffPercent {

    private static final double ABSOLUTE_DIFFERENCE = 100.0d;

    static double diff(File image, File otherImage) throws IOException {
        BufferedImage img1 = ImageIO.read(image);
        BufferedImage img2 = ImageIO.read(otherImage);

        if (img1 == null || img2 == null) {
            return ABSOLUTE_DIFFERENCE;
        }

        return getDifferencePercent(img1, img2);
    }

    private static double getDifferencePercent(BufferedImage img1, BufferedImage img2) {
        int width = img1.getWidth();
        int height = img1.getHeight();
        int width2 = img2.getWidth();
        int height2 = img2.getHeight();

        if (width != width2 || height != height2) {
            return ABSOLUTE_DIFFERENCE;
        }

        long diff = 0;
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                diff += pixelDiff(img1.getRGB(x, y), img2.getRGB(x, y));
            }
        }

        long maxDiff = 3L * 255 * width * height;
        return 100.0 * diff / maxDiff;
    }

    private static int pixelDiff(int rgb1, int rgb2) {
        int r1 = (rgb1 >> 16) & 0xff;
        int g1 = (rgb1 >> 8) & 0xff;
        int b1 = rgb1 & 0xff;
        int r2 = (rgb2 >> 16) & 0xff;
        int g2 = (rgb2 >> 8) & 0xff;
        int b2 = rgb2 & 0xff;
        return abs(r1 - r2) + abs(g1 - g2) + abs(b1 - b2);
    }

}