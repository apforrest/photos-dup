Image organiser attempts to reorganise images by moving them from a source
location to a given destination. Images will be structured under year and
month, and the images renamed to include their creation date and time.

The source and destination locations can be the same. In this case it'll
simply reorganise those images that fall out of the mentioned structure.

If an image with the same name is found, then it shall be checked for
being a duplicate. If not, an incrementing integer value is added to the
file name, otherwise, the image is not moved across.

To build this project you need to run:
`mvn clean install`

Once the jar is built run:
`java -jar ImageOrganiser-1.0-SNAPSHOT-jar-with-dependencies.jar -s /source -d /destination`
